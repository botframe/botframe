package de.solusvoid.jsonhandler;

import de.solusvoid.exceptions.CustomException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A class representing the sortie JSON object from the warframe-api
 */
public class Sortie {

    private static final Logger LOGGER = LogManager.getLogger(Sortie.class);

    private final JSONObject jsonObject;
    private final List<String> sorties;

    public Sortie(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
        this.sorties = extractSortie();
    }

    /**
     * Extracts the sortie information from the JSON object received from the warframe-api
     *
     * @return A list of Strings containing the relevant sortie information
     */
    private List<String> extractSortie() {
        List<String> sortie = new ArrayList<>();

        sortie.add("`Current Sortie:`" + "\n");

        for (Object o : jsonObject.getJSONArray("variants")) {
            JSONObject jsonObjectSortie = (JSONObject) o;
            sortie.add("**Mission type:** " + jsonObjectSortie.getString("missionType"));
            sortie.add("**Modifier:** " + jsonObjectSortie.getString("modifier"));
            sortie.add("**Modifier description:** " + jsonObjectSortie.getString("modifierDescription") + "\n");
        }

        LOGGER.info("Extracted sortie: {}", sortie);

        return sortie;
    }

    /**
     * Custom formatting for the toString method to properly display the message
     *
     * @return A formatted String with all the fissure information
     */
    @Override
    public String toString() {
        if (sorties != null) {
            StringBuilder sortie = new StringBuilder();
            for (String sortieDetail : sorties) {
                sortie.append(sortieDetail).append("\n");
            }
            return sortie.toString();
        } else {
            throw new CustomException("The sortie object is null");
        }
    }
}

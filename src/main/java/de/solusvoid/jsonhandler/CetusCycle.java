package de.solusvoid.jsonhandler;

import org.json.JSONObject;

/**
 * A class representing the cetusCycle JSON object from the warframe-api
 */
public class CetusCycle {

    private final JSONObject cetusCycleJson;
    private final String shortString;

    public CetusCycle(JSONObject cetusCycleJson) {
        this.cetusCycleJson = cetusCycleJson;
        this.shortString = extractShortString();
    }

    /**
     * Extracts the shortString information from the JSON object received from the warframe-api
     *
     * @return A String containing the shortString information from the JSON object,
     * where the 'm' has been replaced with 'minutes'
     */
    private String extractShortString() {
        return "`Cetus cycle:` " + "**" + (cetusCycleJson.getString("shortString").replace("m", " minutes")) + "**";
    }

    public String getShortString() {
        return shortString;
    }
}

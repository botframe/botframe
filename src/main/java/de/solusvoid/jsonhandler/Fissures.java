package de.solusvoid.jsonhandler;

import de.solusvoid.domain.Fissure;
import de.solusvoid.exceptions.CustomException;
import org.apache.commons.lang.time.DurationFormatUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

/**
 * A class representing the fissures JSON object from the warframe-api
 */
public class Fissures {

    private static final Logger LOGGER = LogManager.getLogger(Fissures.class);
    public static final String MISSION_TYPE = "missionType";

    private final JSONArray jsonArray;
    private final List<String> fissureInfo;

    public Fissures(JSONArray jsonArray, String parameter) {
        this.jsonArray = jsonArray;
        this.fissureInfo = new ArrayList<>();
        fissureInfo.addAll(extractRelevantFissures(parameter));
    }

    public Fissures(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
        this.fissureInfo = new ArrayList<>();
        fissureInfo.addAll(extractAllFissures());
    }

    /**
     * Extracts the fissure information matching the fissureTypes parameter from the JSON Array received from the warframe-api
     *
     * @param parameter Comma separated String containing the type of fissures that should be shown
     * @return A list of strings containing the relevant information for each fissure matching the filter criteria
     */
    private List<String> extractRelevantFissures(String parameter) {
        List<String> relevantFissures = new ArrayList<>();

        List<Fissure> fissureList = new ArrayList<>();

        Set<String> parameterList = new HashSet<>();

        if (parameter.contains("(")) {
            parameterList = splitFissureTypes(parameter);
        } else if (parameter.contains("-")) {
            parameterList = extractMissionTier(parameter);
        }


        if (!parameter.isEmpty()) {
            for (Fissure fissure : getAllFissures()) {
                for (String parameterFromList : parameterList) {
                    if (parameterFromList.equalsIgnoreCase(fissure.getMissionType())
                            || parameterFromList.equalsIgnoreCase(fissure.getTier().getParameter())) {
                        fissureList.add(fissure);
                    }
                }
            }
        }

        relevantFissures.add("`Currently active fissures matching these filters: " + parameterList + "`" + "\n");

        relevantFissures.addAll(convertFissuresToStringList(fissureList));

        if (relevantFissures.size() <= 2) {
            relevantFissures.clear();
            relevantFissures.add("**No fissures found matching these filter criteria:** " + parameterList);
        }

        LOGGER.info("Extracted active fissures: {} matching these filters: {}", relevantFissures, parameterList);

        return relevantFissures;
    }

    /**
     * Extracts all fissures from the JSON Array received from the warframe-api
     *
     * @return A list of Strings containing all active fissure missions
     */
    private List<String> extractAllFissures() {
        List<String> allFissures = new ArrayList<>();

        List<Fissure> fissureList = getAllFissures();

        allFissures.add("`All currently active fissures:`");
        allFissures.add("\n");

        allFissures.addAll(convertFissuresToStringList(fissureList));

        return allFissures;
    }

    /**
     * Extracts all fissures from the json array and converts them to {@link Fissure} objects
     *
     * @return A list of {@link Fissure} sorted by mission tier
     */
    private List<Fissure> getAllFissures() {
        List<Fissure> fissureList = new ArrayList<>();

        for (Object o : jsonArray) {
            JSONObject jsonObject = (JSONObject) o;

            Fissure fissure = new Fissure(jsonObject.getString(MISSION_TYPE),
                    jsonObject.getString("tier"),
                    jsonObject.getString("enemy"),
                    jsonObject.getString("eta"),
                    jsonObject.getString("node"));

            fissureList.add(fissure);
        }

        fissureList.sort(Comparator.comparing(Fissure::getTier)
                .thenComparing(Fissure::getEnemyFaction)
                .thenComparing(Fissure::getTimeLeft));

        return fissureList;
    }

    /**
     * Converts a list of {@link Fissure} to a list of Strings
     *
     * @param fissureList List of {@link Fissure} to be converted
     * @return A list strings representing the {@link Fissure} object with some additional description
     */
    private List<String> convertFissuresToStringList(List<Fissure> fissureList) {
        List<String> fissureStringList = new ArrayList<>();

        for (Fissure fissure : fissureList) {
            fissureStringList.add("**Mission type:** " + fissure.getMissionType());
            fissureStringList.add("**Tier:** " + fissure.getTier().getValue());
            fissureStringList.add("**Node:** " + fissure.getNode());
            fissureStringList.add("**Enemy faction:** " + fissure.getEnemyFaction());
            fissureStringList.add("**Time left:** " + DurationFormatUtils.formatDuration(fissure.getTimeLeft().toMillis(), "HH:mm:ss", true) + "\n");
        }

        LOGGER.info("Extracted all active fissures: {}", fissureStringList);

        return fissureStringList;
    }

    /**
     * Custom formatting for the toString method to properly display the message
     *
     * @return A formatted String with all the fissure information
     */
    @Override
    public String toString() {
        if (!fissureInfo.isEmpty()) {
            StringBuilder allFissures = new StringBuilder();
            for (String fissure : fissureInfo) {
                allFissures.append(fissure).append("\n");
            }
            return allFissures.toString();
        } else {

            LOGGER.error("Could not execute to string because the fissure list is empty");

            throw new CustomException("The fissures list is empty");
        }
    }

    /**
     * Splits a comma separated String into single Strings that are added to a String list
     *
     * @param fissureTypes Comma separated String containing fissure types
     * @return A list of Strings containing fissure types from the param
     */
    private Set<String> splitFissureTypes(String fissureTypes) {
        String fissureTypeSubstring = fissureTypes.substring(fissureTypes.indexOf('(') + 1, fissureTypes.indexOf(')'));
        String[] splitFissureTypesArray = fissureTypeSubstring.split(",");

        LOGGER.info("Split fissure types: {}", Arrays.asList(splitFissureTypesArray));

        List<String> fissureTypesList = new ArrayList<>(Arrays.asList(splitFissureTypesArray));
        Set<String> fissureTypesListTrimmed = new HashSet<>();

        fissureTypesList.forEach(s -> fissureTypesListTrimmed.add(s.trim()));

        return fissureTypesListTrimmed;
    }


    /**
     * Splits a space separated String into single Strings that are added to a String list
     *
     * @param missionTier Space separated String containing mission tiers
     * @return A list of Strings containing mission tiers from the param
     */
    private Set<String> extractMissionTier(String missionTier) {
        String missionTierSubstring = missionTier.substring(missionTier.indexOf('-'));
        String[] splitFissureTypesArray = missionTierSubstring.split(" ");

        LOGGER.info("Split mission tiers: {}", Arrays.asList(splitFissureTypesArray));

        return new HashSet<>(Arrays.asList(splitFissureTypesArray));
    }
}

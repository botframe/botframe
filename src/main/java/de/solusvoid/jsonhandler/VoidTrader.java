package de.solusvoid.jsonhandler;

import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Data
public class VoidTrader {
    private static final Logger LOGGER = LogManager.getLogger(VoidTrader.class);

    private final JSONObject voidTrader;
    private final String voidTraderInfo;

    public VoidTrader(JSONObject voidTrader) {
        this.voidTrader = voidTrader;
        this.voidTraderInfo = extractVoidTraderInfo();
    }

    public String extractVoidTraderInfo() {
        return "`Void Trader:` \n\n"
                + "**Arrival:** " + convertLocalDateTimeToCurrentTimeZone(voidTrader.getString("activation"))
                + " (" + voidTrader.getString("startString")
                + ")\n"
                + "**Location:** " + voidTrader.getString("location")
                + "\n"
                + "**Departure:** " + convertLocalDateTimeToCurrentTimeZone(voidTrader.getString("expiry"))
                + " (" + voidTrader.getString("endString") + ")"
                + "\n"
                + "\n"
                + ((voidTrader.getJSONArray("inventory").isEmpty()) ? "" : extractInventory(voidTrader.getJSONArray("inventory")));
    }

    private String extractInventory(JSONArray jsonArray) {
        StringBuilder inventory = new StringBuilder();

        inventory.append("`Inventory:` \n\n");

        for (Object o : jsonArray) {
            JSONObject jsonObject = (JSONObject) o;
            inventory.append("**Item:** ").append(jsonObject.getString("item")).append("\n");
            inventory.append("**Ducats:** ").append(jsonObject.getInt("ducats")).append("\n");
            inventory.append("**Credits:** ").append(jsonObject.getInt("credits")).append("\n");
            inventory.append("\n");
        }

        return inventory.toString();
    }

    /**
     * Replaces the 'Z' from a String date in the UTC timezone and converts it to a LocalDateTime with Europe/Berlin timezone.
     *
     * @param date A date in the UTC timezone format
     * @return A String with the parameter formatted to a german date standard adjusted for timezone offset
     */
    private String convertLocalDateTimeToCurrentTimeZone(String date) {
        LocalDateTime localDateTime = LocalDateTime.parse(date.replace("Z", ""));
        ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.of("UTC"));

        LocalDateTime convertedLocalDateTime = zonedDateTime.withZoneSameInstant(ZoneId.of("Europe/Berlin")).toLocalDateTime();

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

        return convertedLocalDateTime.format(dateTimeFormatter);
    }
}

package de.solusvoid.jsonhandler;

import org.json.JSONObject;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

/**
 * A class representing the arbitration JSON object from the warframe-api
 */
public class Arbitration {

    private final JSONObject jsonObject;
    private final String arbitrationInfo;

    public Arbitration(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
        arbitrationInfo = extractArbitration();
    }

    /**
     * Extracts the arbitration details from the arbitration JSON object received from the warframe-api
     *
     * @return String containing the arbitration details mission type, enemy faction, mission node, time left
     */
    private String extractArbitration() {
        JSONObject arbitration = (JSONObject) jsonObject.get("arbitration");

        return "`Currently active arbitration: `" +
                "\n \n" +
                "**Mission type:** " +
                arbitration.getString("type") +
                "\n" +
                "**Enemy faction:** " +
                arbitration.getString("enemy") +
                "\n" +
                "**Mission node:** " +
                arbitration.getString("node") +
                "\n" +
                "**Time left:** " +
                convertExpiryToDuration(arbitration.getString("expiry"));
    }


    /**
     * Calculates duration between the current UTC date and the expiry parameter
     *
     * @param expiry String representing the arbitration's expiry
     * @return String with the minutes between the given parameter and LocalDateTime.now at UTC
     */
    private String convertExpiryToDuration(String expiry) {
        LocalDateTime ldtExpiry = LocalDateTime.parse(expiry.replace("Z", ""));

        LocalDateTime localDateTime = LocalDateTime.now(ZoneOffset.UTC);

        long minutesBetween = ChronoUnit.MINUTES.between(localDateTime, ldtExpiry);

        return minutesBetween + " minutes";
    }

    public String getArbitrationInfo() {
        return arbitrationInfo;
    }
}

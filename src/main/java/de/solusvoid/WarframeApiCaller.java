package de.solusvoid;

import de.solusvoid.exceptions.CustomException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * A class used to call the warframe-api
 */
class WarframeApiCaller {

    private static final Logger LOGGER = LogManager.getLogger(WarframeApiCaller.class);

    private final String specificInfoUrl;
    private HttpURLConnection connection;

    WarframeApiCaller(String specificInfoUrl) throws IOException {
        this.specificInfoUrl = specificInfoUrl;
        initializeConnection();
    }

    /**
     * Initializes the connection to the warframe-api and checks if the site returns http status code 200
     *
     * @throws IOException when the connection to the warframe-api cannot be established
     */
    private void initializeConnection() throws IOException {
        URL url = new URL("https://api.warframestat.us/pc/" + specificInfoUrl);

        connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        connection.setRequestMethod("GET");
        connection.connect();
        int responseCode = connection.getResponseCode();

        if (responseCode != 200) {
            LOGGER.error("Connection to the Warframe API failed. Http response code: {}", responseCode);
            throw new CustomException("HttpResponseCode: " + responseCode);
        }
        LOGGER.info("Successfully established a connection to the Warframe API");
    }

    /**
     * Handles JSONObject responses from the warframe-api
     *
     * @return JSONObject representing the warframe-api JSONObject
     * @throws IOException when the input does not contain information
     */
    JSONObject getResponse() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        if (!response.toString().trim().isEmpty()) {
            return new JSONObject(response.toString());
        } else {
            LOGGER.error("Response from the Warframe API is null");
            throw new CustomException("Response is null: " + response);
        }

    }

    /**
     * Handles JSONArray responses from the warframe-api
     *
     * @return A JSONArray representing the warframe-api JSONArray
     * @throws IOException when the input stream does not contain information
     */
    JSONArray getResponseArray() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        if (!response.toString().trim().isEmpty()) {
            return new JSONArray(response.toString());
        } else {
            LOGGER.error("Response from the Warframe API is null");
            throw new CustomException("Response is null: " + response);
        }

    }
}

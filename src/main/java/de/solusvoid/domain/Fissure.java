package de.solusvoid.domain;

import de.solusvoid.constants.FissureMissionTier;
import lombok.Data;

import java.time.Duration;

@Data
public class Fissure {
    private String missionType;
    private FissureMissionTier tier;
    private String enemyFaction;
    private Duration timeLeft;
    private String node;


    public Fissure(String missionType, String tier, String enemyFaction, String timeLeft, String node) {
        this.missionType = missionType;
        this.tier = FissureMissionTier.valueOf(tier.toUpperCase());
        this.enemyFaction = enemyFaction;
        this.timeLeft = parseDurationFromString(timeLeft);
        this.node = node;
    }


    /**
     * Converts a String with the format 1h 30m 20s to a {@link Duration} object.
     * The input String gets split at the space character then the letters h m and s get replaced with an empty String.
     * The split Strings get converted to int and then converted to seconds and added up.
     *
     * @param durationString String to be converted
     * @return {@link Duration} object with the Strings information.
     */
    private Duration parseDurationFromString(String durationString) {
        short hours = 0;
        short minutes = 0;
        short seconds = 0;

        long durationInSeconds;

        String[] timeUnits = durationString.split(" ");

        for (String timeUnit : timeUnits) {
            if (timeUnit.contains("h")) {
                hours = Short.parseShort(timeUnit.replace("h", "").trim());
            } else if (timeUnit.contains("m")) {
                minutes = Short.parseShort(timeUnit.replace("m", "").trim());
            } else if (timeUnit.contains("s")) {
                seconds = Short.parseShort(timeUnit.replace("s", "").trim());
            }
        }

        durationInSeconds = (long) (hours * 3600) + (minutes * 60) + seconds;

        return Duration.ofSeconds(durationInSeconds);
    }
}

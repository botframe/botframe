package de.solusvoid;

import de.solusvoid.constants.EBotframeCommands;
import de.solusvoid.exceptions.CustomException;
import de.solusvoid.jsonhandler.*;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.channel.TextChannel;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Optional;
import java.util.Random;

/*
BOTFRAME_INFOS_CHANNEL_ID = 556443440108535810
BOTFRAME_DEV_CHANNEL_ID = 565129688280858625
*/

public class BotframeMain {

    private static final Logger LOGGER = LogManager.getLogger(BotframeMain.class);
    private static final String WARFRAME_API_CALLER_ERROR = "Warframe API caller initialization failed";
    private static TextChannel botframeInfos = null;
    private static Random random = null;
    private static String token;
    private static long botframeInfosChannelId = -1;
    private static final Options options = new Options();

    public static void main(String[] args) {

        /* Set command line options */
        setCommandLineOptions();

        /* Bot Token for authentication */
        token = "NTU2NDI2NjU0ODIyNjk0OTI0.D25nnQ.JS2zE-yaOqVV1f2hqyV06AS0OtU";

        /* Read command line arguments */
        readCommandLineOptions(args);

        /* Check if the channel id is not the default value of -1. If so, exit the application */
        if (botframeInfosChannelId == -1) {
            LOGGER.error("Channel id was not set via command line. Application will shut down.");
            System.exit(1);
        }

        DiscordApi api = new DiscordApiBuilder().setToken(token).login().join();
        LOGGER.info("Successfully connected to the Discord API");

        Optional<TextChannel> optionalTextChannel = api.getTextChannelById(botframeInfosChannelId);

        if (optionalTextChannel.isPresent()) {
            botframeInfos = optionalTextChannel.get();
            LOGGER.info("Successfully found and connected to the text channel with id: {} ", botframeInfos.getId());
        } else {
            LOGGER.error("Text channel with id: {} could not be found or connected to", botframeInfos.getId());
            throw new CustomException("Botframe-infos channel not found");
        }

        /* This seemingly useless assignment happens, because variables in lambda expressions should be final */
        final long finalBotframeInfosChannelId = botframeInfosChannelId;


        /* Posts a message based on what command has been used */
        api.addMessageCreateListener(event -> {

            /* Check if the command was called from the designated command channel */
            if (event.getChannel().getId() == finalBotframeInfosChannelId) {

                String messageContent = event.getMessageContent();

                if (EBotframeCommands.matchesAnyCommand(messageContent)) {

                    callCommand(EBotframeCommands.identifyCommand(messageContent), messageContent);

                    /* If the message does not contain a valid command post a list of all commands */
                } else if (!event.getMessageAuthor().isYourself()) {
                    event.getMessage().addReaction("⚠️");
                    botframeInfos.sendMessage("'" + messageContent + "' is not a command. \n \n"
                            + EBotframeCommands.getAllCommandsAsString());
                }

                /* Post a message if commands are being used outside the designated channel */
            } else if (EBotframeCommands.matchesAnyCommand(event.getMessageContent())) {
                event.getChannel().sendMessage("Please use the designated channel 'botframe-infos' to trigger commands.");
            }
        });

    }

    /**
     * Fetches the current cetus cycle from the warframe-api
     *
     * @return {@link CetusCycle}
     */
    private static CetusCycle cetusCommand() {
        WarframeApiCaller cetusWarframeApiCaller = null;
        try {
            cetusWarframeApiCaller = new WarframeApiCaller("cetusCycle");
        } catch (IOException e) {
            LOGGER.error(WARFRAME_API_CALLER_ERROR, e);
        }
        CetusCycle cetusCycle = null;
        try {
            assert cetusWarframeApiCaller != null;
            cetusCycle = new CetusCycle(cetusWarframeApiCaller.getResponse());
        } catch (IOException e) {
            LOGGER.error("Cetus Cycle initialization failed", e);
        }

        if (cetusCycle != null) {
            return cetusCycle;
        } else {
            throw new CustomException("Cetus Cycle is null");
        }
    }

    /**
     * Fetches all fissures that match the given fissure types from the warframe-api
     *
     * @param parameter types of fissures that should be displayed
     * @return {@link Fissures}
     */
    private static Fissures fissureCommand(String parameter) {
        WarframeApiCaller fissureWarframeApiCaller = null;
        try {
            fissureWarframeApiCaller = new WarframeApiCaller("fissures");
        } catch (IOException e) {
            LOGGER.error(WARFRAME_API_CALLER_ERROR, e);
        }
        Fissures fissures = null;
        try {
            assert fissureWarframeApiCaller != null;
            fissures = new Fissures(fissureWarframeApiCaller.getResponseArray(), parameter);
        } catch (IOException e) {
            LOGGER.error("Fissures initialization failed", e);
        }

        if (fissures != null) {
            return fissures;
        } else {
            throw new CustomException("Fissures is null");
        }
    }

    /**
     * Fetches all fissures that match the given fissure types from the warframe-api
     *
     * @return {@link Fissures}
     */
    private static Fissures fissureCommand() {
        WarframeApiCaller fissureWarframeApiCaller = null;
        try {
            fissureWarframeApiCaller = new WarframeApiCaller("fissures");
        } catch (IOException e) {
            LOGGER.error(WARFRAME_API_CALLER_ERROR, e);
        }
        Fissures fissures = null;
        try {
            assert fissureWarframeApiCaller != null;
            fissures = new Fissures(fissureWarframeApiCaller.getResponseArray());
        } catch (IOException e) {
            LOGGER.error("Fissures initialization failed", e);
        }

        if (fissures != null) {
            return fissures;
        } else {
            throw new CustomException("Fissures is null");
        }
    }

    /**
     * Fetches the current sortie missions from the warframe-api
     *
     * @return {@link Sortie}
     */
    private static Sortie sortieCommand() {
        WarframeApiCaller sortieWarframeApiCaller = null;
        try {
            sortieWarframeApiCaller = new WarframeApiCaller("sortie");
        } catch (IOException e) {
            LOGGER.error(WARFRAME_API_CALLER_ERROR, e);
        }
        Sortie sortie = null;
        try {
            assert sortieWarframeApiCaller != null;
            sortie = new Sortie(sortieWarframeApiCaller.getResponse());
        } catch (IOException e) {
            LOGGER.error("Sortie initialization failed", e);
        }

        if (sortie != null) {
            return sortie;
        } else {
            throw new CustomException("Sortie is null");
        }
    }

    /**
     * Fetches arbitration from the warframe-api
     *
     * @return {@link Arbitration}
     */
    private static Arbitration arbitrationCommand() {
        WarframeApiCaller arbitrationWarframeApiCaller = null;
        try {
            arbitrationWarframeApiCaller = new WarframeApiCaller("");
        } catch (IOException e) {
            LOGGER.error(WARFRAME_API_CALLER_ERROR, e);
        }
        Arbitration arbitration = null;
        try {
            assert arbitrationWarframeApiCaller != null;
            arbitration = new Arbitration(arbitrationWarframeApiCaller.getResponse());
        } catch (IOException e) {
            LOGGER.error("Arbitration initialization failed", e);
        }

        if (arbitration != null) {
            return arbitration;
        } else {
            throw new CustomException("Arbitration is null");
        }
    }

    /**
     * Calculates a random number between 50 and 99
     *
     * @return Int between 50 and 99
     */
    private static int brojet7ShareholdCommand() {
        try {
            random = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Random initialization failed.", e);
        }
        int low = 50;
        int high = 99;
        return random.nextInt(high - low) + low;
    }

    private static VoidTrader voidTraderCommand() {
        WarframeApiCaller voidTraderWarframeApiCaller = null;
        try {
            voidTraderWarframeApiCaller = new WarframeApiCaller("voidTrader");
        } catch (IOException e) {
            LOGGER.error(WARFRAME_API_CALLER_ERROR, e);
        }
        VoidTrader voidTrader = null;
        try {
            assert voidTraderWarframeApiCaller != null;
            voidTrader = new VoidTrader(voidTraderWarframeApiCaller.getResponse());
        } catch (IOException e) {
            LOGGER.error("Void trader initialization failed", e);
        }

        if (voidTrader != null) {
            return voidTrader;
        } else {
            throw new CustomException("Void trader is null");
        }
    }


    /**
     * Calls a bot command according to the command parameter
     *
     * @param command         bot command that should be called
     * @param originalMessage original message containing the bot command and additional information
     */
    private static synchronized void callCommand(String command, String originalMessage) {

        /* Command for the Cetus day/night cycle */
        if (command.equals(EBotframeCommands.CETUS.name())) {
            botframeInfos.sendMessage(cetusCommand().getShortString());
            return;
        }

        /* Command for all fissures matching the filter criteria */
        if (command.equals(EBotframeCommands.FISSURES_WITH_PARAM.name())) {
            botframeInfos.sendMessage(fissureCommand(originalMessage).toString());
            return;
        }

        /* Command for all fissures matching from one specific tier */
        if (command.equals(EBotframeCommands.FISSURES_TIER_FILTER.name())) {
            botframeInfos.sendMessage(fissureCommand(originalMessage).toString());
            return;
        }

        /* Command for all fissures */
        if (command.equals(EBotframeCommands.FISSURES.name())) {
            botframeInfos.sendMessage(fissureCommand().toString());
            return;
        }

        /* Command for the current Sortie mission */
        if (command.equals(EBotframeCommands.SORTIE.name())) {
            botframeInfos.sendMessage(sortieCommand().toString());
            return;
        }

        /* Command for brojet7's randomly calculated sharehold between 50% and 99% */
        if (command.equals(EBotframeCommands.BROJET7SHAREHOLD.name())) {
            botframeInfos.sendMessage("`Brojet7's aktueller Aktienanteil bei Digital Extremes:` **"
                    + brojet7ShareholdCommand() + "%**");
            return;
        }

        /* Command for listing all commands */
        if (command.equals(EBotframeCommands.COMMANDS.name())) {
            botframeInfos.sendMessage(EBotframeCommands.getAllCommandsAsString());
            return;
        }

        /* Command for the current arbitration */
        if (command.equals(EBotframeCommands.ARBITRATION.name())) {
            botframeInfos.sendMessage(arbitrationCommand().getArbitrationInfo());
        }

        if (command.equals(EBotframeCommands.VOID_TRADER.name())) {
            botframeInfos.sendMessage(voidTraderCommand().getVoidTraderInfo());
        }

        /* Command candidate did not match any command */
        if (command.equals("Command candidate did not match any command")) {
            throw new CustomException("Command candidate did not match any command." +
                    " This might indicate some error in the code," +
                    " because the matchesAnyCommand method should have prevented the execution of this here code.");
        }
    }

    /**
     * Configures all available command line options
     */
    private static void setCommandLineOptions() {
        options.addRequiredOption("id", "channel-id", true, "Channel id the bot connects to");
        options.addOption("dev", "development-mode", false, "Start in development mode");
    }


    /**
     * Parses command line options
     *
     * @param args Application launch option passed on from the main method
     */
    private static void readCommandLineOptions(String[] args) {
        CommandLineParser commandLineParser = new DefaultParser();
        try {
            CommandLine commandLine = commandLineParser.parse(BotframeMain.options, args);

            if (commandLine.hasOption("id")) {
                botframeInfosChannelId = Long.parseLong(commandLine.getOptionValue("id"));
                if (commandLine.hasOption("dev")) {
                    token = "NTY1MTMxMDE3Mjk2MjgxNjAx.XKyA2g.GYfrmVEwKTUST1m8-NnE1JUxA5w";
                }
            } else {
                LOGGER.error("Channel id was not set via command line. Application will shut down.");
                System.exit(1);
            }
        } catch (ParseException e) {
            LOGGER.error(e);
        }
    }
}


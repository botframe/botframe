package de.solusvoid.constants;

public enum FissureMissionTier {
    LITH("Lith", "-l"),
    MESO("Meso", "-m"),
    NEO("Neo", "-n"),
    AXI("Axi", "-a"),
    REQUIEM("Requiem", "-r");

    private final String value;
    private final String parameter;

    FissureMissionTier(String value, String parameter) {
        this.value = value;
        this.parameter = parameter;
    }

    public String getValue() {
        return value;
    }

    public String getParameter() {
        return parameter;
    }
}

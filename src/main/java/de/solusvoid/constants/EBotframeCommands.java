package de.solusvoid.constants;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * An enum containing all bot commands
 */
public enum EBotframeCommands {
    CETUS(new HashSet<>(Arrays.asList(Pattern.compile("!cetus"), Pattern.compile("!c"))), "**!cetus | !c** -- Current cetus cycle."),
    ARBITRATION(new HashSet<>(Arrays.asList(Pattern.compile("!arbitration"), Pattern.compile("!a"))), "**!arbitration | !a** -- Current arbitration"),
    FISSURES_WITH_PARAM(new HashSet<>(Arrays.asList(Pattern.compile("!fissures[(][a-zA-Z, ]*[)]"), Pattern.compile("!f[(][a-zA-Z, ]*[)]"))), "**!fissures(mission type 1, mission type 2, mission type x) | !f(mission type 1, mission type 2, mission type x)** -- Lists all fissures with the mission type matching the arguments in the parenthesis. E.g. !fissures(survival) to list all survival missions"),
    FISSURES(new HashSet<>(Arrays.asList(Pattern.compile("!fissures"), Pattern.compile("!f"))), "**!fissures | !f** -- Lists all fissures"),
    FISSURES_TIER_FILTER(new HashSet<>(Arrays.asList(Pattern.compile("!fissures( -[lmnar])+"), Pattern.compile("!f( -[lmnar])+"))), "**!fissures -<mission-tier> | !f -<mission-tier>** -- Lists all fissures from a specified tier. E.g. !fissures -m to list all Meso fissures"),
    SORTIE(new HashSet<>(Arrays.asList(Pattern.compile("!sortie"), Pattern.compile("!s"))), "**!sortie | !s** -- Current sortie"),
    VOID_TRADER(new HashSet<>(Arrays.asList(Pattern.compile("!trader"), Pattern.compile("!t"))), "**!trader | !t** -- Void trader status"),
    BROJET7SHAREHOLD(new HashSet<>(Arrays.asList(Pattern.compile("!brojet7sharehold"), Pattern.compile("!b"))), "**!brojet7sharehold | !b** -- Brojet7's current share hold at Digital Extremes"),
    COMMANDS(new HashSet<>(Arrays.asList(Pattern.compile("!commands"), Pattern.compile("!com"))), "**!commands | !com** -- Lists all commands");

    private final Set<Pattern> command;
    private final String commandDescription;

    EBotframeCommands(Set<Pattern> command, String commandDescription) {
        this.commandDescription = commandDescription;
        this.command = command;
    }

    /**
     * Checks if a given String matches any of the bot commands
     *
     * @param input A String to be matched against the command strings
     * @return true if the string matches one of the bot commands
     */
    public static boolean matchesAnyCommand(String input) {
        return matchesPattern(CETUS, input)
                || matchesPattern(ARBITRATION, input)
                || matchesPattern(FISSURES, input)
                || matchesPattern(SORTIE, input)
                || matchesPattern(BROJET7SHAREHOLD, input)
                || matchesPattern(FISSURES_WITH_PARAM, input)
                || matchesPattern(FISSURES_TIER_FILTER, input)
                || matchesPattern(VOID_TRADER, input)
                || matchesPattern(COMMANDS, input);
    }

    /**
     * Checks if a given the input parameter matches the given EBotFrameCommand
     *
     * @param eBotframeCommand EBotframeCommand to match against
     * @param input            Input to check for a match
     * @return true if the input matches the EBotframeCommand
     */
    private static boolean matchesPattern(EBotframeCommands eBotframeCommand, String input) {
        for (Pattern pattern : eBotframeCommand.command) {
            if (pattern.matcher(input).matches()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Formats all commands as String
     *
     * @return A String containing all commands, neatly formatted for better readability
     */
    public static String getAllCommandsAsString() {

        StringBuilder commands = new StringBuilder();

        commands.append("`List of all commands:`");
        commands.append("\n\n");

        EBotframeCommands[] eBotframeCommands = EBotframeCommands.values();
        for (EBotframeCommands botframeCommands : eBotframeCommands) {
            commands.append(botframeCommands.commandDescription).append("\n");
        }
        return commands.toString();
    }


    /**
     * Find out which command has been used.
     *
     * @param commandCandidate message that possibly is a command
     * @return String with the command's commandPattern.toString()
     */
    public static String identifyCommand(String commandCandidate) {

        /* Command for the Cetus day/night cycle */
        if (matchesPattern(CETUS, commandCandidate)) {
            return CETUS.name();
        }

        /* Command for all fissures matching the filter criteria */
        if (matchesPattern(FISSURES_WITH_PARAM, commandCandidate)) {
            return FISSURES_WITH_PARAM.name();
        }

        /* Command for all fissures of one specific mission tier */
        if (matchesPattern(FISSURES_TIER_FILTER, commandCandidate)) {
            return FISSURES_TIER_FILTER.name();
        }

        /* Command for all fissures */
        if (matchesPattern(FISSURES, commandCandidate)) {
            return FISSURES.name();
        }

        /* Command for the current Sortie mission */
        if (matchesPattern(SORTIE, commandCandidate)) {
            return SORTIE.name();
        }

        /* Command for brojet7's randomly calculated sharehold between 50% and 99% */
        if (matchesPattern(BROJET7SHAREHOLD, commandCandidate)) {
            return BROJET7SHAREHOLD.name();
        }

        /* Command for listing all commands */
        if (matchesPattern(COMMANDS, commandCandidate)) {
            return COMMANDS.name();
        }

        /*Command for the current arbitration*/
        if (matchesPattern(ARBITRATION, commandCandidate)) {
            return ARBITRATION.name();
        }

        /*Command for the void trader status*/
        if (matchesPattern(VOID_TRADER, commandCandidate)) {
            return VOID_TRADER.name();
        }

        return "Command candidate did not match any command";
    }
}
